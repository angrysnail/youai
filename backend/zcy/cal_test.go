package zcy

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestAdd(t *testing.T) {
	Convey("测试加法", t, func() {
		So(Add(1, 2), ShouldEqual, 3)
		So(Add(1, 0), ShouldEqual, 1)
	})
}

func TestSub(t *testing.T) {
	Convey("测试减法", t, func() {
		So(Sub(3, 4), ShouldEqual, -1)
	})
}

func TestDiv(t *testing.T) {
	Convey("测试除法", t, func() {
		_, err := Div(2, 0)
		So(err, ShouldNotBeNil)
		result, _ := Div(4, 3)
		So(result, ShouldEqual, float32(4/3))
	})
}
