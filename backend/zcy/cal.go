package zcy

import "errors"

func Add(a, b int) int {
	return a + b
}

func Sub(a, b int) int {
	return a - b
}

func Mul(a, b int) int {
	return a * b
}

func Div(a, b int) (float32, error) {
	if b == 0 {
		return 0, errors.New("被除数不可以为0")
	}
	return float32(a / b), nil
}
