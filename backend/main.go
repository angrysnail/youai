package main

import (
	_ "gitee.com/angrysnail/youai/backend/routers"
	_ "gitee.com/angrysnail/youai/backend/sysinit"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}
