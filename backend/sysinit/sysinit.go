package sysinit

import (
	"gitee.com/angrysnail/youai/backend/utils"
	"github.com/astaxie/beego"
)

//package中的init方法会在package加载时执行
func init() {
	//启用Session
	beego.BConfig.WebConfig.Session.SessionOn = true
	//初始化日志
	utils.InitLogs()
	//初始化缓存
	utils.InitCache()
	//初始化数据库
	InitDatabase()
}
