package models

import "github.com/astaxie/beego/orm"

type Child struct {
	Id      int      `form:id`
	Name    string   `form:Name`
	Male    int      `form:Male`
	Address string   `form:Address`
	Married bool     `form:Married`
	Parent  []*Elder `orm:"reverse(many)"`
	//Mother  *Elder `orm:"rel(fk)"`
	//Elder  *Elder `orm:"rel(fk)"`
}

func (a *Child) TableName() string {
	return ChildTBName()
}

//进行孩子的查询
func ChildOne(id int) (*Child, error) {
	o := orm.NewOrm()
	child := Child{Id: id}
	err := o.Read(&child)
	if err != nil {
		return nil, err
	}
	return &child, nil
}
