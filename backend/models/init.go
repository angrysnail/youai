package models

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

//初始化
func init() {
	orm.RegisterModel(&Elder{}, &Child{})
}

//下面是统一的表名管理
func TableName(name string) string {
	prefix := beego.AppConfig.String("db_dt_prefix")
	return prefix + name
}

//获取 Elder 对应的表名称
func ElderTBName() string {
	return TableName("elder")
}

//获取 Child 对应的表名称
func ChildTBName() string {
	return TableName("child")
}
