package models

import (
	"github.com/astaxie/beego/orm"
)

type ElderQueryparam struct {
	BaseQueryParam
	Elder
}

type Elder struct {
	Id       int      `form:"Id"`
	Name     string   `form:"Name"`
	Age      int      `form:Age`
	Address  string   `form:Address`
	Male     bool     `form:Male`
	Alone    bool     `form:Alone`
	Children []*Child `orm:"rel(m2m)"`
}

func (a *Elder) TableName() string {
	return ElderTBName()
}

// 进行老年人的查询
func ElderOne(id int) (*Elder, error) {
	o := orm.NewOrm()
	elder := Elder{Id: id}
	err := o.Read(&elder)
	if err != nil {
		return nil, err
	}
	return &elder, nil
}

// 进行老年人的列表查询
func ElderList(param *ElderQueryparam) (*[]*Elder, error) {
	o := orm.NewOrm()
	qs := o.QueryTable(&Elder{})
	if param.Name != "" {
		qs.Filter("name__contains", param.Name)
	}
	qs.Filter("male__exact", param.Male)
	qs.Filter("alone__exact", param.Alone)
	all := &[]*Elder{}
	_, err := qs.All(all)
	return all, err
}

//进行老年人的更新
func ElderUpdate(elder *Elder) (int, error) {
	return 0, nil
}

// 进行老年人的删除
func ElderDelete(id int64) (int, error) {
	return 0, nil
}
