package models

import (
	"fmt"
	"github.com/astaxie/beego/orm"
)

func testInsert() {
	elder := Elder{
		Name:    "elderName",
		Age:     1,
		Address: "address",
		Male:    true,
		Alone:   true,
	}
	o := orm.NewOrm()
	// 插入其所有的子女
	child1 := &Child{
		Name:    "child1",
		Sex:     1,
		Married: false,
	}
	child2 := &Child{
		Name:    "child1",
		Sex:     1,
		Married: false,
	}
	o.Insert(child1)
	o.Insert(child2)
	elder.Children = append(elder.Children, child1, child2)
	id, err := o.Insert(elder)
	if err != nil {
		fmt.Print("wrong when insert elder", id)
		return
	} else if id > 0 {
		fmt.Print("insert successful", id)
	}
}

func main() {
	testInsert()
}

//type User struct {
//	Id          int
//	Name        string
//	Profile     *Profile   `orm:"rel(one)"` // OneToOne relation
//	Post        []*Post `orm:"reverse(many)"` // 设置一对多的反向关系
//}
//
//type Profile struct {
//	Id          int
//	Age         int16
//	User        *User   `orm:"reverse(one)"` // 设置一对一反向关系(可选)
//}
//
//type Post struct {
//	Id    int
//	Title string
//	User  *User  `orm:"rel(fk)"`    //设置一对多关系
//	Tags  []*Tag `orm:"rel(m2m)"`
//}
//
//type Tag struct {
//	Id    int
//	Name  string
//	Posts []*Post `orm:"reverse(many)"`
//}
