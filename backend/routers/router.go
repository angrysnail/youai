package routers

import (
	"gitee.com/angrysnail/youai/backend/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/elder/?:id", &controllers.ElderController{}, "Get:Get")
	beego.Router("/elder", &controllers.ElderController{}, "Post:Insert")
	beego.Router("/elder", &controllers.ElderController{}, "Get:List")
	beego.Router("/elder/children/?:id", &controllers.ElderController{}, "POST:AddChildren")
}
